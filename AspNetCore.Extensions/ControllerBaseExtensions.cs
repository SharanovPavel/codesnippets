﻿using Microsoft.AspNetCore.Http;
using System;

namespace Microsoft.AspNetCore.Mvc
{
    public static class ControllerBaseExtensions
    {
        private static IActionResult HandleExceptionByDefault(this ControllerBase controller, Exception e)
        {
            return controller.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }

        public static IActionResult InvokeInLayout<T>(this ControllerBase controller, Action action, Func<Exception, IActionResult> exceptionHandler = null)
        {
            try
            {
                action();
                return controller.Ok();
            }
            catch (Exception e)
            {
                return exceptionHandler?.Invoke(e) ?? controller.HandleExceptionByDefault(e);
            }
        }

        public static IActionResult InvokeInLayout<T>(this ControllerBase controller, Func<T> function, Func<Exception, IActionResult> exceptionHandler = null)
        {
            try
            {
                return controller.Ok(function());
            }
            catch (Exception e)
            {
                return exceptionHandler?.Invoke(e) ?? controller.HandleExceptionByDefault(e);
            }
        }
    }
}
