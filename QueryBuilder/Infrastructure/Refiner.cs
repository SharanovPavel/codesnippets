﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace QueryBuilder.Infrastructure
{
    public class Refiner : ExpressionVisitor
    {
        protected bool RefineTrue { get; }
        protected ParameterExpression Parameter { get; }
        public Refiner(ParameterExpression parameter, bool refineTrue = true)
        {
            RefineTrue = refineTrue;
            Parameter = parameter ?? throw new ArgumentNullException(nameof(parameter));
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return Parameter;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (RefineTrue && 
               (node.NodeType == ExpressionType.AndAlso || node.NodeType == ExpressionType.OrElse))
            {
                if (node.Left is ConstantExpression cl &&
                    cl.Value.Equals(true))
                {
                    return Visit(node.Right);
                }
                else
                {
                    if (node.Right is ConstantExpression cr &&
                        cr.Value.Equals(true))
                    {
                        return Visit(node.Left);
                    }
                }
            }

            return node.Update(Visit(node.Left), null, Visit(node.Right));
        }
    }
}
