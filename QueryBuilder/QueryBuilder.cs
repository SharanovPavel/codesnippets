﻿using QueryBuilder.Infrastructure;
using System;
using System.Linq.Expressions;

namespace QueryBuilder
{
    public class QueryBuilder<T>
    {
        protected static Expression EmptyQueryExpression => Expression.Constant(true);
        protected ParameterExpression Parameter { get; }

        protected Expression Aggregator { get; set; } = EmptyQueryExpression;

        public Expression<Func<T, bool>> Object => Expression.Lambda<Func<T, bool>>(Refine(Aggregator), new[] { Parameter });

        public QueryBuilder()
        {
            Parameter = Expression.Parameter(typeof(T), "arg");
        }

        public QueryBuilder<T> Query(Expression<Func<T, bool>> query)
        {
            return Fluent(
                () => Aggregator = query.Body);
        }

        public QueryBuilder<T> And(Expression<Func<T, bool>> right)
        {
            return Fluent(
                () => Aggregator = Expression.AndAlso(Aggregator, right.Body));
        }

        public QueryBuilder<T> Or(Expression<Func<T, bool>> right)
        {
            return Fluent(
                () => Aggregator = Expression.OrElse(Aggregator, right.Body));
        }

        public QueryBuilder<T> Reset()
        {
            return Fluent(
                () => Aggregator = EmptyQueryExpression);
        }

        private QueryBuilder<T> Fluent(Action action)
        {
            action();
            return this;
        }

        private Expression Refine(Expression expression)
        {
            return new Refiner(Parameter).Visit(Aggregator);
        }
    }
}
