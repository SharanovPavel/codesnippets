----------Simple Query Builder----------
------------------by--------------------
------------Pavel Sharanov--------------

-----------------Usage------------------

var builder = new QueryBuilder<string>();

var strings = new List<string>()
{
	"Halo",
	"",
	"Hello, world!",
	"Bonjour!"
}

builder
	.Query(str => str.Length > 0)
	.And(str => str.Trim().StartsWith("Hell"))
	.Or(str => str.Length == 4);

var query = builder.Object; //arg => (arg.Length > 0 && arg.Trim().StartsWith("Hell")) || arg.Length == 4

var result = strings.AsQueryable().Where(query).ToList();
//["Halo", "Hello, world!"]